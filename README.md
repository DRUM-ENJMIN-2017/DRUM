# DRUM

DRUM (Defend Recruit Upgrade Move), est un jeu de stratégie en temps réel où le joueur contrôle son armée par des commandes textuelles.
Le but du jeu est de survivre le plus longtemps, une IA envoie régulièrement des soldats sur des points à défendre. Le joueur doit donc
gérer ses ressources pour pouvoir recruter des soldats et envoyer ces derniers pour défendre des zones. Lorsque les deux points sont
occupés par les soldats ennemis en même temps, la partie s'achève. Le score du joueur correspond au temps qu'il a survécu.
Des informations sur l'état du jeu sont données sur la partie gauche de l'écran, et la partie droite donne des informations textuelles.

## Commandes
Les commandes en jeu sont :
* RECRUIT <number of humans>
* MOVE <number of humans> FROM <A|B> TO <A|B>
* DEFEND  <A | B> <number of humans>
* UPGRADE <GOLD|WOOD|A|B>
* TRADE <number of ressource wanted> <WOOD|GOLD>         

NB :
* Durant une partie, un joueur peut mettre le jeu en pause avec le touche "CTRL"    
* La touche "TAB" permet d'autocompléter les commandes

## Ressources et bâtiments
Le joueur possède 3 ressources :
* De l'or
* Du bois
* Des soldats

L'or et le bois lui servent à recruter des soldats

Le joueur peut intéragir avec 4 bâtiments:
* une mine qui lui rapporte de l'or
* une scierie qui lui rapporte du bois
* Un point "A" à défendre
* Un point "B" à défendre

Chacun des bâtiments peut être amélioré (UPGRADE):
* Mine et Scierie : augmente la capacité maximale de la ressource, et produit plus vite
* Points "A" et "B" : augmente le nombre maximal de soldats que le joueur peut mettre sur ce point
