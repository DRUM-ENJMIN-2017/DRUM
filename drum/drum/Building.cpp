#include "Building.hpp"
#include "Resource.hpp"
#include "Globals.hpp"

#include <cassert>
#include <string>

// Constructor
Building::Building(Scene& p_owner, size_t p_x, size_t p_y, size_t p_baseWoodCost, size_t p_baseGoldCost) :
    Entity(p_owner, p_x, p_y),
    m_isActivated(true),
    m_processDuration(0),
    m_level(1),
    m_baseWoodCost(p_baseWoodCost),
    m_baseGoldCost(p_baseGoldCost),
    m_color(COLOR_TEXT_WHITE),
	m_isFull(false)
{
}

// Update
void Building::update(double dt)
{
    if (m_isActivated)
    {
        m_processDuration += dt;
    }
}

// Return upgrade cost (wood, gold)
std::pair<size_t, size_t> Building::getUpgradeCost(void) const
{
    return std::make_pair(m_baseWoodCost * static_cast<size_t>(std::pow(UPGRADE_COEFFICIENT, m_level - 1)), m_baseGoldCost * static_cast<size_t>(std::pow(2, m_level - 1)));
}

void Building::draw(CHAR_INFO p_buffer[SCREEN_HEIGHT][SCREEN_WIDTH])
{
    const size_t patternHeight = m_visual.size();
    auto& costPair = getUpgradeCost();
    m_visual[patternHeight - 2] = "Level  : " + std::to_string(m_level);
    m_visual[patternHeight - 1] = "Upgrade: (" + std::to_string((int)(costPair.first)) + " wood, " + std::to_string((int)(costPair.second)) + " gold)";
	
    for (size_t lineNumber = 0; lineNumber < patternHeight; ++lineNumber)
    {
		int tmpColor = m_color;
        size_t size = m_visual[lineNumber].size();

		// If the player can upgrade the building, the line of uprgade info will be displayed in green
		if (lineNumber == (patternHeight - 1)) // line upgrade info
		{
			if ((float)(costPair.first) <= Resource::get()->wood && (float)(costPair.second) <= Resource::get()->gold)
			{
				tmpColor = COLOR_TEXT_GREEN;
			}
		}

		// If the player can upgrade the building, the line of uprgade info will be displayed in green
		if (lineNumber == (patternHeight - 3)) // line upgrade info
		{
			if (m_isFull)
			{
				tmpColor = COLOR_TEXT_RED;
			}
		}

        for (size_t i = 0; i < size; ++i)
        {
            p_buffer[m_y + lineNumber][m_x + i].Char.AsciiChar = m_visual[lineNumber][i];
            p_buffer[m_y + lineNumber][m_x + i].Attributes = tmpColor;
        }
    }

}

// Level Up
void Building::levelUp(void)
{
    auto& cost = getUpgradeCost();
    Resource::get()->wood -= cost.first;
    Resource::get()->gold -= cost.second;
    ++m_level;
    
    //TODO : Dialog print message !
}

void Building::setVisual(std::vector<std::string> p_visual)
{
    m_visual = p_visual;

    // Add the three lines containing the UI
    m_visual.emplace_back();
    m_visual.emplace_back();
}

// Destructor
Building::~Building()
{
}
