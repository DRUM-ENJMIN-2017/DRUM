#ifndef BUILDING_HPP
#define BUILDING_HPP

#include "Entity.hpp"

constexpr float UPGRADE_COEFFICIENT = 2.f;

class Building : public Entity
{
public:
    // Constructor
    explicit Building(Scene& p_owner, size_t p_x, size_t p_y,
        size_t p_baseWoodCost, size_t p_baseGoldCoste);

    // Entity::Update
    virtual void update(double dt) override; //c++11
    virtual void draw(CHAR_INFO p_buffer[SCREEN_HEIGHT][SCREEN_WIDTH]) override;
    virtual std::pair<size_t, size_t> getUpgradeCost(void) const;
    virtual void setVisual(std::vector<std::string> p_visual) override;
    virtual void levelUp(void);

    // Destructor
    virtual ~Building();

protected:
    bool m_isActivated;
	bool m_isFull;
    double m_processDuration;
    size_t m_level;
    size_t m_baseWoodCost;
    size_t m_baseGoldCost;
    int m_color;
};

#endif //  BUILDING_HPP