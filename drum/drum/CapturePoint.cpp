#include "CapturePoint.hpp"

#include <string>

// Constructor
CapturePoint::CapturePoint(Scene& p_owner, size_t p_x, size_t p_y, size_t p_baseWoodCost, size_t p_baseGoldCost, size_t p_baseMaxCapacity) :
    Building(p_owner, p_x, p_y, p_baseWoodCost, p_baseGoldCost),
    m_baseMaxCapacity(p_baseMaxCapacity),
    m_allyForces(0),
    m_enemyForces(0),
    m_isOccupied(false)
{
}

// Update at each frame the entity
void CapturePoint::update(double dt)
{
    Building::update(dt);

    if (m_processDuration >= DECREASE_PERIOD)
    {
        m_processDuration -= DECREASE_PERIOD;
        
        // Decrease periodicly
        if (m_allyForces > 0 && m_enemyForces > 0)
        {
            --m_allyForces;
            --m_enemyForces;
        }
    }

    if (m_enemyForces > m_allyForces)
    {
        m_occupationDuration += dt;
		if (m_occupationDuration > 10)
		{
			m_occupationDuration = 12;
		}
        
		// Make it blink
        m_color = (int)(m_occupationDuration * 10) % 2 == 0 || m_occupationDuration > 10
            ? COLOR_TEXT_RED
            : COLOR_TEXT_PINK;

        if (m_occupationDuration > OCCUPATION_DURATION)
        {
            m_isOccupied = true;
		}
		else {
			m_isOccupied = false;
		}
    }
    else if (m_enemyForces == 0)
    {
        m_color = COLOR_TEXT_WHITE;
        m_occupationDuration = 0;
		m_isOccupied = false;
    }
    else
    {
        m_color = COLOR_TEXT_YELLOW;
        m_occupationDuration = 0;
		m_isOccupied = false;

    }
}

// Drawing
void CapturePoint::draw(CHAR_INFO p_buffer[SCREEN_HEIGHT][SCREEN_WIDTH])
{
    m_visual[m_visual.size() - 3] = std::to_string(m_allyForces) + " VS " + std::to_string(m_enemyForces) + " (" + std::to_string(getCurrentCapacity ()) +" places)";
    
	if (m_occupationDuration > 0 && m_occupationDuration <= 10)
	{
		m_visual[m_visual.size() - 4] = "Being Captured ("
			+ std::to_string((int)m_occupationDuration) + "s/"
			+ std::to_string((int)OCCUPATION_DURATION) + "s)";
	}
	else if (m_occupationDuration > 10)
	{
		m_visual[m_visual.size() - 4] = "Occupied by the enemy !";
	}
	else
    {
        if (m_allyForces > m_enemyForces)
            m_visual[m_visual.size() - 4] = "This point is yours.";
        else if (m_allyForces < m_enemyForces)
            m_visual[m_visual.size() - 4] = "Occupied by the enemy !";
        else
            m_visual[m_visual.size() - 4] = "Unoccupied";
    }

    Building::draw(p_buffer);
}

// Set visual
void CapturePoint::setVisual(std::vector<std::string> p_visual)
{
    Building::setVisual(p_visual);

    // Add the third line containing the 12 VS 32
    m_visual.emplace_back();
    m_visual.emplace_back();
}

// Get current capacity
size_t CapturePoint::getCurrentCapacity(void) const
{
    return m_baseMaxCapacity * static_cast<size_t>(pow(2, m_level - 1));
}

// Destructor
CapturePoint::~CapturePoint()
{
}
