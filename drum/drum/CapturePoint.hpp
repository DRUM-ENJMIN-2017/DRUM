#ifndef CAPTUREPOINT_HPP
#define CAPTUREPOINT_HPP

#include "Building.hpp"

// The time to lose a 1/1 unit
constexpr double DECREASE_PERIOD = 0.5f;

// Time over which the eny has taken the capture point
constexpr double OCCUPATION_DURATION = 10.f;

class CapturePoint : public Building
{
public:
    //Constructor
    CapturePoint(Scene& p_owner, size_t p_x, size_t p_y,
        size_t p_baseWoodCost, size_t p_baseGoldCost, size_t p_baseMaxCapacity);

    // Building
    void update(double dt) override;
    void draw(CHAR_INFO p_buffer[SCREEN_HEIGHT][SCREEN_WIDTH]) override;
    void setVisual(std::vector<std::string> p_visual) override;

    // Inline getters
    inline size_t getAllyForces(void) const { return m_allyForces; }
    inline size_t getEnemyForces(void) const { return m_enemyForces; }
    inline bool isOccupied(void) const {return m_isOccupied;}
    size_t getCurrentCapacity(void) const;

    // Inline setters
    inline void addAllyForces(size_t p_value) { m_allyForces += p_value; }
    inline void addEnemyForces(size_t p_value) {m_enemyForces += p_value;}
    inline void removeAllyForces(size_t p_value) { m_allyForces -= p_value; }
    inline void removeEnemyFroces(size_t p_value) { m_enemyForces -= p_value; }

    // Destructor
    virtual ~CapturePoint();

protected:
    size_t m_baseMaxCapacity;
    size_t m_allyForces;
    size_t m_enemyForces;

    bool m_isOccupied;
    double m_occupationDuration;
};

#endif // CAPTUREPOINT_HPP