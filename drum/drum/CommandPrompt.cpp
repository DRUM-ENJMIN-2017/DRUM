#include "CommandPrompt.hpp"

#include "Resource.hpp"
#include "Game.hpp"
#include "Building.hpp"
#include "CapturePoint.hpp"


#include <conio.h>
#include <stdio.h>
#include <algorithm>
#include <sstream>


// Constructor
CommandPrompt::CommandPrompt(Game&  p_owner) :
	m_input(" "),
	m_owner(p_owner)
{
	std::ifstream inf;
	std::string word;

	// Load the dico with the words to autocomplete
	inf.open("dico.txt");

	while (!inf.eof())
	{
		inf >> word;
		m_dicoCommands.push_back(word);
	}

	m_isThreadRunning.test_and_set();
}

void CommandPrompt::previousInput()
{
	m_mutex.lock();
	if (m_inputHistory.size() > 0)
	{
		m_historyIndex = max(0, m_historyIndex - 1);
		m_input = m_inputHistory[m_historyIndex];
	}
	m_mutex.unlock();
}

void CommandPrompt::nextInput()
{
	m_mutex.lock();

	m_historyIndex = min(m_inputHistory.size(), m_historyIndex + 1);
	if (m_historyIndex >= m_inputHistory.size())
	{
		m_input = "";
	}
	else {
		m_input = m_inputHistory[m_historyIndex];
	}
	m_mutex.unlock();
}

void CommandPrompt::autoCompleteInput()
{
	m_mutex.lock();
	std::istringstream iss(m_input);
	std::string word;
	std::vector<std::string> currentInputWords;
	std::string lastWord;

	// We split the current input by space
	while (getline(iss, word, ' ')) {
		currentInputWords.push_back(word);
	}
	
	if (currentInputWords.size() > 0)
	{
		lastWord = currentInputWords[currentInputWords.size() - 1];

		for (std::vector<std::string>::iterator it = m_dicoCommands.begin(); it != m_dicoCommands.end(); ++it)
		{
			if (lastWord == (*it).substr(0, lastWord.length()))
			{
				lastWord = (*it);
				break;
			}
		}

		// now we recreate the input from the vector
		currentInputWords[currentInputWords.size() - 1] = lastWord;
		m_input = "";
		for (std::vector<std::string>::iterator it = currentInputWords.begin(); it != currentInputWords.end(); ++it)
		{
			m_input += (*it) + " ";
		}
	}

	m_mutex.unlock();
}

// Launches the thread
void CommandPrompt::launchThread(void)
{
	Log::get()->write("[CommandPrompt]:\tLaunching thread.");
	m_thread.reset(new std::thread(&CommandPrompt::listen, this)); // From here, thread is launched,
}

// Kill the thread
void CommandPrompt::killThread(void)
{
	Log::get()->write("[CommandPrompt]:\tKilling thread.");
	m_isThreadRunning.clear();
}


// Thread 
void CommandPrompt::listen(void)
{
	m_isThreadRunning.test_and_set(); // put it to true at first

	FlushConsoleInputBuffer(GetStdHandle(STD_INPUT_HANDLE)); // flush input buffer

	while (m_isThreadRunning.test_and_set() == true)
	{
		if (!m_gamePaused && _kbhit())
		{
			// Get keyboard input
			char letter;
			letter = _getch(); // conio.h
			letter = toupper(letter); // uppercase

			if (!m_gamePaused)
			{
				// Lock m_input resource
				m_mutex.lock();

				if (letter == '\r')
				{
					interpretAndApply();
					m_input.clear(); // send event
				}
				else if (letter == BACKSPACE_CHAR)
				{
					if (m_input.empty() == false)
						m_input.pop_back();
				}
				else if (letter == ' ' || (letter >= '0' && letter <= '9') || (letter >= 'a' && letter <= 'z') || (letter >= 'A' && letter <= 'Z'))
				{
					if (m_input.size() < MAX_INPUT_LENGTH)
						m_input += letter;
				}

				m_mutex.unlock();
				// unlocked 
			}
		}
	}

	//std::terminate(); // force quit ?
}

// Interpret the input
void CommandPrompt::interpretAndApply(void)
{
	Log::get()->write("[CommandPrompt]:\tInterpreting command : " + m_input + ".");

	std::string errorMessage = "I don't understand your order my lord. Type 'HELP' to get the list of all the commands. ";
	std::string synthax = "Please use : ";

	Dialog& dialog = m_owner.getDialog();

	auto& words = splitStr(m_input, " ");

	if (words.size() == 0)
	{
		dialog.addMessage(errorMessage, COLOR_TEXT_BLUE_GREEN);
		return; // Spawn error
	}

	// RECRUIT N
	if (words[0] == "RECRUIT")
	{
		std::string synthax = "RECRUIT <number of humans>";
		if (words.size() != 2)
		{
			dialog.addMessage(errorMessage + synthax, COLOR_TEXT_BLUE_GREEN);
			return; // spawn error
		}

		// Get value
		int value;
		std::istringstream toInt(words[1]);
		toInt >> std::dec >> value;

		// Error
		if (toInt.fail() == true)
		{
			dialog.addMessage(errorMessage + synthax, COLOR_TEXT_BLUE_GREEN);
			return;
		}

		// Check if enought resources
		size_t totalWoodCost = HUMAN_WOOD_COST * value;
		size_t totalGoldCost = HUMAN_GOLD_COST * value;

		if (Resource::get()->gold < totalGoldCost)
		{
			dialog.addMessage("You don't have enought " + ENTITIES_TO_RESSOURCE_STR[EEntities::GOLD] + " to recruit " + words[1] + " humans", COLOR_TEXT_PURPLE);
			return;
		}

		if (Resource::get()->wood < totalWoodCost)
		{
			dialog.addMessage("You don't have enought " + ENTITIES_TO_RESSOURCE_STR[EEntities::WOOD] + " to recruit " + words[1] + " humans", COLOR_TEXT_PURPLE);
			return;
		}

		// Spend it
		Resource::get()->wood -= totalWoodCost;
		Resource::get()->gold -= totalGoldCost;
		Resource::get()->humans += value;
		dialog.addMessage("You have recruited " + words[1] + " humans", COLOR_TEXT_GREY_BLUE);
	}
	else if (words[0] == "UPGRADE") // UPGRADE [GOLD|WOOD|A|B]
	{
		synthax += "UPGRADE <GOLD|WOOD|A|B>";
		if (words.size() != 2)
		{
			dialog.addMessage(errorMessage + synthax, COLOR_TEXT_BLUE_GREEN);
			return; // spawn errors
		}
		if (words[1] == "GOLD")
		{
			Building* building = static_cast<Building*>(m_owner.getEntity(EEntities::GOLD));

			auto& costPair = building->getUpgradeCost();
			if (Resource::get()->wood >= costPair.first
				&& Resource::get()->gold >= costPair.second)
			{
				building->levelUp();
				dialog.addMessage(EMessageType::Upgrade, EEntities::GOLD);
			}
			else {
				dialog.addMessage("You don't have enought ressource to upgrade your " + ENTITIES_TO_BUIDLING_STR[EEntities::GOLD], COLOR_TEXT_PURPLE);
			}

		}
		else if (words[1] == "WOOD")
		{
			Building* building = static_cast<Building*>(m_owner.getEntity(EEntities::WOOD));

			auto& costPair = building->getUpgradeCost();
			if (Resource::get()->wood >= costPair.first
				&& Resource::get()->gold >= costPair.second)
			{
				building->levelUp();
				dialog.addMessage(EMessageType::Upgrade, EEntities::WOOD);
			}
			else {
				dialog.addMessage("You don't have enought ressource to upgrade your " + ENTITIES_TO_BUIDLING_STR[EEntities::GOLD], COLOR_TEXT_PURPLE);
			}

		}
		else if (words[1] == "A")
		{
			Building* building = static_cast<Building*>(m_owner.getEntity(EEntities::A_CONTROL_POINT));

			auto& costPair = building->getUpgradeCost();
			if (Resource::get()->wood >= costPair.first
				&& Resource::get()->gold >= costPair.second)
			{
				building->levelUp();
				dialog.addMessage(EMessageType::Upgrade, EEntities::A_CONTROL_POINT);
			}
			else {
				dialog.addMessage("You don't have enought ressource to upgrade your " + ENTITIES_TO_BUIDLING_STR[EEntities::A_CONTROL_POINT], COLOR_TEXT_PURPLE);
			}

		}
		else if (words[1] == "B")
		{
			Building* building = static_cast<Building*>(m_owner.getEntity(EEntities::B_CONTROL_POINT));

			auto& costPair = building->getUpgradeCost();
			if (Resource::get()->wood >= costPair.first
				&& Resource::get()->gold >= costPair.second)
			{
				building->levelUp();
				dialog.addMessage(EMessageType::Upgrade, EEntities::B_CONTROL_POINT);
			}
			else {
				dialog.addMessage("You don't have enought ressource to upgrade your " + ENTITIES_TO_BUIDLING_STR[EEntities::B_CONTROL_POINT], COLOR_TEXT_PURPLE);
			}

		}
		else {
			dialog.addMessage(errorMessage + synthax, COLOR_TEXT_BLUE_GREEN);
		}


	}
	else if (words[0] == "MOVE")
	{
		synthax += "MOVE <number of humans> FROM <A|B> TO <A|B>";
		EEntities fromBuilding;
		EEntities toBuilding;

		// MOVE b FROM A TO B
		if (words.size() != 6)
		{
			dialog.addMessage(errorMessage + synthax, COLOR_TEXT_BLUE_GREEN);
			return; // error
		}

		if (words[2] != "FROM" || words[4] != "TO")
		{
			dialog.addMessage(errorMessage + synthax, COLOR_TEXT_BLUE_GREEN);
			return; // illformed
		}

		std::string from = words[3];
		std::string to = words[5];

		// Get value
		size_t value;
		std::istringstream toInt(words[1]);
		toInt >> std::dec >> value;

		// Error not int
		if (toInt.fail() == true)
		{
			dialog.addMessage(errorMessage + synthax, COLOR_TEXT_BLUE_GREEN);
			return;
		}

		fromBuilding = (from == "A" ? EEntities::A_CONTROL_POINT : EEntities::B_CONTROL_POINT);
		toBuilding = (to == "A" ? EEntities::A_CONTROL_POINT : EEntities::B_CONTROL_POINT);

		CapturePoint* fromPoint = static_cast<CapturePoint*>(m_owner.getEntity(from == "A" ? EEntities::A_CONTROL_POINT : EEntities::B_CONTROL_POINT));
		CapturePoint* toPoint = static_cast<CapturePoint*>(m_owner.getEntity(to == "A" ? EEntities::A_CONTROL_POINT : EEntities::B_CONTROL_POINT));

		size_t fromAllyForces = fromPoint->getAllyForces();

		if (value > fromAllyForces)
		{
			dialog.addMessage("You don't have enought unit from this point", COLOR_TEXT_PURPLE);
			return; // Error not enought unit from this point
		}

		size_t toMaxAllyforces = toPoint->getCurrentCapacity();
		size_t toAllyForces = toPoint->getAllyForces();

		if (toAllyForces + value > toMaxAllyforces)
		{
			dialog.addMessage("You don't have enought room in the destination point", COLOR_TEXT_PURPLE);
			return; // Not enought room in destination point.
		}

		fromPoint->removeAllyForces(value);
		toPoint->addAllyForces(value);
		dialog.addMessage(EMessageType::Move, toBuilding, EEntities::SOLDIER, value);

	}
	else if (words[0] == "DEFEND")
	{
		synthax += "DEFEND  <A|B> <number of humans>";
		EEntities buildingEnum;

		// DEFEND A n
		if (words.size() != 3)
		{
			dialog.addMessage(errorMessage + synthax, COLOR_TEXT_BLUE_GREEN);
			return;
		}

		std::string buildingName = words[1];
		CapturePoint* building = nullptr;

		if (buildingName == "A")
		{
			building = static_cast<CapturePoint*>(m_owner.getEntity(EEntities::A_CONTROL_POINT));
			buildingEnum = EEntities::A_CONTROL_POINT;
		}
		else if (buildingName == "B")
		{
			building = static_cast<CapturePoint*>(m_owner.getEntity(EEntities::B_CONTROL_POINT));
			buildingEnum = EEntities::B_CONTROL_POINT;
		}
		else
		{
			dialog.addMessage(errorMessage + synthax, COLOR_TEXT_BLUE_GREEN);
			return; // error wrong arg
		}

		size_t maxAlly = building->getCurrentCapacity();
		size_t allyForces = building->getAllyForces();

		// Get value
		size_t value;
		std::istringstream toInt(words[2]);
		toInt >> std::dec >> value;

		// Error
		if (toInt.fail() == true)
		{
			dialog.addMessage(errorMessage + synthax, COLOR_TEXT_BLUE_GREEN);
			return;
		}

		if (Resource::get()->humans < value)
		{
			dialog.addMessage("You don't have enought humans to defend", COLOR_TEXT_PURPLE);
			return; // Error not enought humans
		}

		if (allyForces <= maxAlly)
		{
			size_t delta = min(maxAlly - allyForces, value);
			building->addAllyForces(delta);
			Resource::get()->humans -= delta;

			dialog.addMessage(EMessageType::Defend, buildingEnum, EEntities::SOLDIER, value);
		}
		else {
			dialog.addMessage("You don't have enought room to send your army in the point to defend", COLOR_TEXT_PURPLE);
			return; // Error not enought humans
		}
	}
	else if (words[0] == "HELP")
	{
		dialog.addEmptyLine();
		dialog.addMessage("---------------------------- HELP ---------------------------", COLOR_TEXT_GREY);
		dialog.addMessage("Use : 'UPGRADE <GOLD|WOOD|A|B>' to upgrade your buildings", COLOR_TEXT_GREY);
		dialog.addMessage("Use : 'RECRUIT <number of humans>' to recruit humans", COLOR_TEXT_GREY);
		dialog.addMessage("Use : 'MOVE <number of humans> FROM <A|B> TO <A|B>' to move humans from a point to another", COLOR_TEXT_GREY);
		dialog.addMessage("Use : 'DEFEND  <A|B> <number of humans>' to defend a point with the given number of humans", COLOR_TEXT_GREY);
		dialog.addMessage("Use : 'TRADE  <number of resource> <WOOD|GOLD>' to trade some resource for 80% of the other", COLOR_TEXT_GREY);
		dialog.addMessage("-------------------------------------------------------------", COLOR_TEXT_GREY);

	}
	else if (words[0] == "TRADE")
	{
		synthax += "TRADE <number of ressource wanted> <WOOD|GOLD>";
		if (
			words.size() != 3
			|| (!(words[2] == "WOOD" || words[2] == "GOLD"))
			)
		{
			dialog.addMessage(errorMessage + synthax, COLOR_TEXT_BLUE_GREEN);
			return; // Error not enought humans
		}

		// Get value
		int value;
		std::istringstream toInt(words[1]);
		toInt >> std::dec >> value;

		// Error not int
		if (toInt.fail() == true)
		{
			dialog.addMessage(errorMessage + synthax, COLOR_TEXT_BLUE_GREEN);
			return;
		}

		bool tradeWood = words[2] == "WOOD";
		if (tradeWood == true)
		{
			if (Resource::get()->wood < value)
			{
				dialog.addMessage("You don't have enought wood to trade.", COLOR_TEXT_PURPLE);
				return;
			}

			Resource::get()->gold += (int)(value * TRADE_COEFFICIENT);
			Resource::get()->wood -= (int)(value * TRADE_COEFFICIENT);
		}
		else
		{
			if (Resource::get()->gold < value)
			{
				dialog.addMessage("You don't have enought gold to trade.", COLOR_TEXT_PURPLE);
				return;
			}

			Resource::get()->wood += (int)(value * TRADE_COEFFICIENT);
			Resource::get()->gold -= (int)(value * TRADE_COEFFICIENT);
		}

		dialog.addMessage("You traded " + words[1] + " " + words[2] + " for " + std::to_string((int)(value * TRADE_COEFFICIENT)) + " of the other resource.", COLOR_TEXT_YELLOW);
	}
	else if (words[0] == "SURRENDER")
	{
		m_owner.surrender();
	}
	else {
		dialog.addMessage(errorMessage + "I don't understand the order '" + words[0] + "'", COLOR_TEXT_BLUE_GREEN);
	}

	m_inputHistory.push_back(m_input);
	m_historyIndex = m_inputHistory.size();
}

// Desctructor
CommandPrompt::~CommandPrompt()
{
	killThread();

	if (m_thread != nullptr)
	{
		Log::get()->write("[CommandPrompt]:\tWaiting for the thread to end.");
		m_thread->join(); // Wait for the thread
	}
}

// Split a string into array regarding delim
std::vector<std::string> CommandPrompt::splitStr(const std::string& p_string, const std::string& p_delimiter, const bool& p_removeEmptyEntries)
{
	std::vector<std::string> tokens;

	for (size_t start = 0, end; start < p_string.length(); start = end + p_delimiter.length())
	{
		size_t position = p_string.find(p_delimiter, start);
		end = position != std::string::npos ? position : p_string.length();

		std::string token = p_string.substr(start, end - start);
		/*for (auto& c : token)
			c = toupper(c);*/

		if (!p_removeEmptyEntries || !token.empty())
		{
			tokens.push_back(token);
		}
	}

	// Empty tokens
	if (!p_removeEmptyEntries &&
		(p_string.empty() || endsWith(p_string, p_delimiter)))
	{
		tokens.push_back("");
	}

	// Result
	return tokens;
}

// Utility
bool CommandPrompt::endsWith(const std::string& p_string, const std::string& p_suffix)
{
	return p_string.size() >= p_suffix.size() &&
		p_string.substr(p_string.size() - p_suffix.size()) == p_suffix;
}