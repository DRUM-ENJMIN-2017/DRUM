#ifndef COMMANDPROMPT_HPP
#define COMMANDPROMPT_HPP

#include <string>
#include <thread> // c++11
#include <atomic>
#include <vector>
#include <memory>
#include <iostream>
#include <mutex>

#include "Globals.hpp"

#define BACKSPACE_CHAR (char)(0x8)

// Command Prompt
static const std::string COMMAND_PROMPT = "> ";

// Max input length
constexpr size_t MAX_INPUT_LENGTH = 35ul;

class Game;

class CommandPrompt
{
public:
    // Constructor
    explicit CommandPrompt(Game& p_owner);

    // Get current input
    inline const std::string& getInput(void) const {return m_input;}
	
	// Set if game is paused
	inline void setGamePaused(bool value) { m_gamePaused = value; }
	
	// Set if game is paused
	inline const bool getGamePaused() const { return m_gamePaused; }

	void previousInput();
	void nextInput();
	void autoCompleteInput();

    // Launch the thread
    void launchThread(void);
    void killThread(void);
    // Destructor
    virtual ~CommandPrompt();


protected:
    void interpretAndApply(void);
    bool endsWith(const std::string& p_string, const std::string& p_suffix);
    std::vector<std::string> splitStr(const std::string& p_string, const std::string& p_delimiter, const bool& p_removeEmptyEntries = true);

    // Thread
    std::unique_ptr<std::thread> m_thread;
    void listen(void);

    // Current line in command prompte
    std::string m_input;
    std::atomic_flag m_isThreadRunning = ATOMIC_FLAG_INIT;
	std::mutex m_mutex;

    // Owner
    Game& m_owner;

	bool m_gamePaused;

	std::vector<std::string> m_inputHistory;
	int m_historyIndex = 0;

	std::vector<std::string> m_dicoCommands;
};

#endif // COMMANDPROMPT_HPP