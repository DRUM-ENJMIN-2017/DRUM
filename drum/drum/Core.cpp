#include "Core.hpp"

#include <iostream>

#include "SplashScreenScene.hpp"
#include "HelpScene.hpp"
#include "Game.hpp"
#include "LoseScene.hpp"
#include "PauseScene.hpp"
#include "HighScoreScene.hpp"
#include "Log.hpp"

// Constructor
Core::Core() :
	m_isRunning(true),
	hOutput((HANDLE)GetStdHandle(STD_OUTPUT_HANDLE)),
	m_rcRegion{ 0, 0, SCREEN_WIDTH - 1, SCREEN_HEIGHT - 1 }
{
	// dt
	m_lastFrameTime = std::chrono::steady_clock::now();	

	// Set first scene
	m_sceneStack.emplace(new SplashScreenScene(*this));

	// Launch game loop
	Log::get()->write("[Core]:\tInitialized, launching game loop...");
	loop();
}

// Main game loop
void Core::loop(void)
{
	while (m_isRunning)
	{
		// Exit condition - Escape to close
		if (GetAsyncKeyState(VK_ESCAPE) & 0x8000)
		{
			Log::get()->write("[Core]:\tEscape key pressed ! Exiting...");
			m_isRunning = false;
		}

		// DT management
		/*auto& nano_now = std::chrono::steady_clock::now();
		auto& nano_past = m_lastFrameTime;
		auto& duration = (nano_now - nano_past);
		auto nano_count = std::chrono::duration_cast<std::chrono::nanoseconds>(duration).count();
		double dt = nano_count / 10e8;*/
		double dt = getElapsedTimeSinceLastFrame() / 1000;

		if (dt > 0.1) //
		{
			// Read the console content
			ReadConsoleOutput(hOutput, (CHAR_INFO *)m_buffer, BUFFER_SIZE,
				BUFFER_COORD, &m_rcRegion);

			/// Scene
			std::shared_ptr<Scene> currentScene = m_sceneStack.top();

			// Is Active ?
			if (currentScene->isActive() == true)
			{
				currentScene->update(dt);
				currentScene->draw(m_buffer);
			}
			else
			{
				// New scene
				Scene* nextScene = nullptr;

				switch (currentScene->getNextScene())
				{
				case EScenes::GameScene:
					m_gameStart = std::chrono::steady_clock::now();
					// First clear buffer
					clearBuffer();
					Log::get()->write("[Core]:\tInstancing new Game scene...");

					nextScene = new Game(*this);
					break;

				case EScenes::HelpScene:

					// First clear buffer
					clearBuffer();
					Log::get()->write("[Core]:\tInstancing new Help scene...");
					nextScene = new HelpScene(*this); // TODO
					break;

					//case EScenes::WinScene:
					//{
					//	Log::get()->write("[Core]:\tInstancing new Win scene...");
					//	//nextScene = new WinScene(*this); // TODO
					//}break;

				case EScenes::LoseScene:

					// First clear buffer
					clearBuffer();
					Log::get()->write("[Core]:\tInstancing new Lose scene...");
					nextScene = new LoseScene(*this, getElapsedTime());
					break;

				case EScenes::SplashscreenScene:

					// First clear buffer
					clearBuffer();
					Log::get()->write("[Core]:\tInstancing new SplashScreen scene...");
					nextScene = new SplashScreenScene(*this);
					break;

				case EScenes::PauseScene:
					Log::get()->write("[Core]:\tInstancing new pause scene...");
					nextScene = new PauseScene(*this);
					m_pauseStart = std::chrono::steady_clock::now();
					break;

				case EScenes::HighScoreScene:
					// First clear buffer
					clearBuffer();
					Log::get()->write("[Core]:\tInstancing new high score scene...");
					nextScene = new HighScoreScene(*this); // TODO
					break;

				}

				// Pop current scene
				size_t numberOfSceneWeNeedToPop = currentScene->mustPopOnExit();
				if (numberOfSceneWeNeedToPop > 0)
				{
					for (size_t i = 0; i < numberOfSceneWeNeedToPop; i++)
					{
						if (m_sceneStack.top()->getEnum() == EScenes::PauseScene) // if pause scene is popped, we need to change the timer
						{
							int pauseDelta = static_cast<int>(std::chrono::duration_cast<std::chrono::seconds>(std::chrono::steady_clock::now() - m_pauseStart).count());
							m_gameStart += std::chrono::seconds(pauseDelta);
						}

						m_sceneStack.pop();
						Log::get()->write("[Core]:\tPoped a scene.");
					}
				}

				// Add new scene if exists
				if (nextScene != nullptr)
				{
					FlushConsoleInputBuffer(GetStdHandle(STD_INPUT_HANDLE));
					Log::get()->write("[Core]:\tPushed a scene");
					m_sceneStack.emplace(nextScene);
				}

				// Exit condition - No more scene
				if (m_sceneStack.size() == 0)
				{
					Log::get()->write("[Core]:\tNo more scene ! Exiting...");
					m_isRunning = false;
				}

				if (m_sceneStack.size() > 0 && nextScene == nullptr)
				{
					m_sceneStack.top()->reactivate();
				}
			}
			///

			// Write the result
			WriteConsoleOutput(hOutput, (CHAR_INFO *)m_buffer, BUFFER_SIZE,
				BUFFER_COORD, &m_rcRegion);

			// Dt
			m_lastFrameTime = std::chrono::steady_clock::now();
		}
	}
}

// Fill the buffer with spaces
void Core::clearBuffer(void)
{
	for (size_t y = 0ul; y < SCREEN_HEIGHT; ++y)
	{
		for (size_t x = 0ul; x < SCREEN_WIDTH; ++x)
		{
			m_buffer[y][x].Char.AsciiChar = ' ';
			m_buffer[y][x].Attributes = 0x0f;
		}

	}
}

// Get elapsed time
int Core::getElapsedTime(void) const
{
	return static_cast<int>(std::chrono::duration_cast<std::chrono::seconds>(std::chrono::steady_clock::now() - m_gameStart).count());
}

double Core::getElapsedTimeSinceLastFrame(void) const
{
	return static_cast<double>(std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now() - m_lastFrameTime).count());
}


// Destructor
Core::~Core()
{
}
