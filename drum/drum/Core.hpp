#ifndef CORE_HPP
#define CORE_HPP

#include <stack>
#include <memory>
#include <chrono>

#include "Scene.hpp"

enum class EScenes 
{
    None,

    SplashscreenScene,
    HelpScene,
    GameScene,
    WinScene,
    LoseScene,
	PauseScene,
	HighScoreScene
};

class Core
{
public:
    // Constructor
    Core();

    // Clear the buffer
    void clearBuffer(void);

    // Return game elapsedTime
    int getElapsedTime(void) const;


	// Return game elapsedTime
	double getElapsedTimeSinceLastFrame(void) const;

    // Destructor
    virtual ~Core();

protected:
    // Game loop
    void loop(void);

    // All the game scenes
    std::stack<std::shared_ptr<Scene> > m_sceneStack;

    // Running
    bool m_isRunning;

    // Console management
    HANDLE hOutput;
    CHAR_INFO m_buffer[SCREEN_HEIGHT][SCREEN_WIDTH];
    SMALL_RECT m_rcRegion;

    // Time Management
    std::chrono::time_point<std::chrono::steady_clock> m_lastFrameTime;
    std::chrono::time_point<std::chrono::steady_clock> m_gameStart;
	std::chrono::time_point<std::chrono::steady_clock> m_pauseStart;
};

#endif // CORE_HPP