#include <sstream>
#include <algorithm>
#include <iostream>
#include <iomanip>

#include "Dialog.hpp"

// Constructor
Dialog::Dialog(Scene& p_owner, size_t p_x, size_t p_y)
	: Entity(p_owner, p_x, p_y)
{
}

// Destructor
Dialog::~Dialog()
{
}

void Dialog::update(double dt)
{
	// Nothing to do
}

void Dialog::draw(CHAR_INFO p_buffer[SCREEN_HEIGHT][SCREEN_WIDTH])
{
	for (size_t i = 0; i < SCREEN_HEIGHT; i++)
	{
		p_buffer[i][m_x - 2].Char.AsciiChar = ' ';
		p_buffer[i][m_x - 2].Attributes = 0xff;
	}

	int minValue = max(((int)m_chat.size() - ((int)SCREEN_HEIGHT-1)), 0);

	for (int line = m_chat.size() - 1; line >= minValue; --line)
	{
		for (size_t j = m_x; j < SCREEN_WIDTH; j++)
		{
			p_buffer[m_y + (line - minValue)][j].Char.AsciiChar = ' ';
		}

		for (size_t i = 0; i < m_chat[line].size(); i++)
		{
			p_buffer[m_y + (line - minValue)][m_x + i] = m_chat[line][i];
		}
	}
}

void Dialog::setVisual(std::vector<std::string> p_visual)
{
	// Nothing to do
}

// Add message to the list of message to display
void Dialog::addMessage(EMessageType p_messageType, EEntities p_building, EEntities p_ressources, size_t p_ressourceNumber, size_t p_minuteLeft, size_t p_secondLeft)
{
	m_color.clear();
	std::stringstream strStream;

	switch (p_messageType)
	{
	case EMessageType::Attack:
		m_color.insert(std::make_pair(18, COLOR_TEXT_WHITE));
		m_color.insert(std::make_pair(25, COLOR_TEXT_BLUE));
		m_color.insert(std::make_pair(29, COLOR_TEXT_WHITE));
		m_color.insert(std::make_pair(34, COLOR_TEXT_GREEN));
		m_color.insert(std::make_pair(100, COLOR_TEXT_WHITE));

		// YOUR ENNEMY ATTACK POINT X IN XX:XX WITH XX SOLDIERS
		strStream << "YOUR ENNEMY ATTACK " << ENTITIES_TO_BUIDLING_STR[p_building] << " IN " << p_minuteLeft << ":" << p_secondLeft << " WITH " << p_ressourceNumber << " " << ENTITIES_TO_RESSOURCE_STR[p_ressources];
		break;

	case EMessageType::BattleWon:
		m_color.insert(std::make_pair(100, COLOR_TEXT_GREY_BLUE));

		// YOU WON THE BATLLE FOR p_building
		strStream << "YOU WON THE BATLLE FOR " << ENTITIES_TO_BUIDLING_STR[p_building];
		break;

	case EMessageType::BattleLost:
		m_color.insert(std::make_pair(100, COLOR_TEXT_RED));

		// YOU LOST THE BATTLE FOR THE POINT p_buidling
		strStream << "YOU LOST THE BATLLE FOR " << ENTITIES_TO_BUIDLING_STR[p_building];
		break;

	case EMessageType::Defend:
		m_color.insert(std::make_pair(31, COLOR_TEXT_WHITE));
		m_color.insert(std::make_pair(100, COLOR_TEXT_BLUE));

		// YOU SEND XXX SOLDIERS TO DEFEND POINT p_building
		strStream << "YOU SEND " << std::setw(3) << p_ressourceNumber << " " << ENTITIES_TO_RESSOURCE_STR[p_ressources] << " TO DEFEND " << ENTITIES_TO_BUIDLING_STR[p_building];
		break;

	case EMessageType::Move:
		m_color.insert(std::make_pair(24, COLOR_TEXT_WHITE));
		m_color.insert(std::make_pair(100, COLOR_TEXT_BLUE));

		// YOU MOVE p_ressourceNumber SOLDIERS TO p_building
		strStream << "YOU MOVE " << std::setw(3) << p_ressourceNumber << " " << ENTITIES_TO_RESSOURCE_STR[p_ressources] << " TO " << ENTITIES_TO_BUIDLING_STR[p_building];
		break;

	case EMessageType::Upgrade:
		m_color.insert(std::make_pair(100, COLOR_TEXT_YELLOW));

		// p_building UPGRADED
		strStream << ENTITIES_TO_BUIDLING_STR[p_building] << " UPGRADED";
		break;
	}

	std::string plainMessage = strStream.str();
	std::vector<CHAR_INFO> chatLine;

	for (size_t i = 0; i < plainMessage.size(); i++)
	{
		CHAR_INFO ci;
		ci.Char.AsciiChar = plainMessage[i];

		// Get color for pos
		std::map<size_t, int>::iterator it;
		it = m_color.lower_bound(i);
		ci.Attributes = it->second;

		chatLine.push_back(ci);
	}

	m_chat.push_back(chatLine);
}

void Dialog::addMessage(std::string textMessage, int color)
{
	int pos = 0;

	while (pos < (int)textMessage.size())
	{
		int lineMessage = min((textMessage.size()-pos), (SCREEN_WIDTH - m_x));
		int linePos = 0;
		std::vector<CHAR_INFO> chatLine;

		for (int linePos = 0; linePos < lineMessage; linePos++)
		{
			CHAR_INFO ci;
			ci.Char.AsciiChar = textMessage[pos];
			ci.Attributes = color;

			chatLine.push_back(ci);
			++pos;
		}

		m_chat.push_back(chatLine);
	}

    /*
	for (size_t i = 0; i < textMessage.size(); i++)
	{
		CHAR_INFO ci;
		ci.Char.AsciiChar = textMessage[i];
		ci.Attributes = color;

		chatLine.push_back(ci);
	}

	m_chat.push_back(chatLine);*/
}

void Dialog::addEmptyLine()
{
	std::vector<CHAR_INFO> chatLine;
	CHAR_INFO ci;

	ci.Char.AsciiChar = ' ';
	ci.Attributes = COLOR_TEXT_WHITE;
	chatLine.push_back(ci);
	m_chat.push_back(chatLine);
}
