#ifndef DIALOG_HPP
#define DIALOG_HPP

#include <string>
#include <vector>
#include <map>

#include "Entity.hpp"
#include "Globals.hpp"

enum EMessageType {
	Attack,
	Defend,
	Move,
	Upgrade,
	BattleWon,
	BattleLost
};

class Dialog : public Entity
{
public:
	// Constructor
	Dialog(Scene& p_owner, size_t p_x, size_t p_y);

    // Add a message
	void addMessage(EMessageType p_messageType, EEntities p_building, EEntities p_ressources = EEntities::SOLDIER, size_t p_ressourceNumber = 0ul, size_t p_minuteLeft = 0ul, size_t p_secondLeft = 0ul);

	void addMessage(std::string textMessage, int color);

	void addEmptyLine();

	// Update : inherit from Entity
	virtual void update(double dt) override;

	// Draw : inherit from Entity
	virtual void draw(CHAR_INFO p_buffer[SCREEN_HEIGHT][SCREEN_WIDTH]) override;

	// Update : set visual
	virtual void setVisual(std::vector<std::string> p_visual) override;

	// Desctructor
	virtual ~Dialog();

protected:
	// All the messages saved
	std::vector<std::vector<CHAR_INFO>> m_chat;
	std::map<size_t, int> m_color;//opti mem
};

#endif