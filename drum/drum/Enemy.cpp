#include "Enemy.hpp"

#include <ctime>
#include <cstdlib>

#include "Log.hpp"
#include "Game.hpp"
#include "CapturePoint.hpp"

// Constructor
Enemy::Enemy(Game& p_owner) :
    Entity(p_owner, -1, -1),
    m_owner(p_owner),
    m_nextWaveArmy(BASE_ENEMY_ARMY)
{
    // Initialize randomness
    std::srand(static_cast<size_t>(std::time(NULL)));

    // Defines actions
    m_actions = {
        // Idle state
        [this]() {
            Dialog& dialog = m_owner.getDialog();
            if (m_firstUpdateInState == true)
            {
                // Initialize random timeout
                dialog.addMessage("The enemy seems to prepare. It should move soon.", COLOR_TEXT_RED);
                m_stateTimeout = rand() % MAX_IDLE_DURATION + MIN_IDLE_DURATION;
            }

            // Exit condition - Timeout
            if (m_stateTime >= m_stateTimeout)
            {
                switch (rand() % 3)
                {
                    case 0:
                        changeState(EStates::Attack_A);
                        break;
                    case 1:
                        changeState(EStates::Attack_B);
                        break;
                    case 2:
                        changeState(EStates::Load);
                        break;
                    }
            }
        },

        // Attack A state
            [this]() {
            Dialog& dialog = m_owner.getDialog();
            if (m_firstUpdateInState == true)
            {
                m_stateTimeout = rand() % MAX_ATTACK_DURATION + MIN_ATTACK_DURATION;
                dialog.addMessage("The enemy seems to be aiming at the A capture point, beware !", COLOR_TEXT_RED);
                dialog.addMessage("> The spies talk about " + std::to_string(m_stateTimeout) + " seconds to prepare yourself.", COLOR_TEXT_RED);
            }

            // Exit condition
            if (m_stateTime >= m_stateTimeout)
            {
                // Put enemy army
                static_cast<CapturePoint*>(m_owner.getEntity(EEntities::A_CONTROL_POINT))->addEnemyForces(m_nextWaveArmy);
                growArmy();

                // Dialog
                dialog.addMessage("The enemy army has reached the A control point, the war has begun !", COLOR_TEXT_RED);
                changeState(EStates::Idle);
            }
        },

            // Attack B State
                [this]() {
                Dialog& dialog = m_owner.getDialog();
                if (m_firstUpdateInState == true)
                {
                    m_stateTimeout = rand() % MAX_ATTACK_DURATION + MIN_ATTACK_DURATION;
                    dialog.addMessage("The enemy seems to be aiming at the B capture point, beware !", COLOR_TEXT_RED);
                    dialog.addMessage("> The spies talk about " + std::to_string(m_stateTimeout) + " seconds to prepare yourself.", COLOR_TEXT_RED);
                }

                // Exit condition
                if (m_stateTime >= m_stateTimeout)
                {
                    // Put enemy army
                    static_cast<CapturePoint*>(m_owner.getEntity(EEntities::B_CONTROL_POINT))->addEnemyForces(m_nextWaveArmy);
                    growArmy();

                    // Dialog
                    dialog.addMessage("The enemy army has reached the B control point, the war has begun !", COLOR_TEXT_RED);
                    changeState(EStates::Idle);
                }
            },

            // Load State
                [this]() {
                Dialog& dialog = m_owner.getDialog();

                // Grow army
                m_nextWaveArmy = static_cast<size_t>(m_nextWaveArmy + m_nextWaveArmy * ARMY_LOADING_COEFFICIENT);

                // Dialog
				dialog.addMessage("My lord, the spies are confused; the enemy general acts oddly, it must be preparing something...", COLOR_TEXT_RED);
                changeState(EStates::Idle); // temp
            }
    };

    // Initialize state machine
    changeState(EStates::Idle);
    applyChangeState();
}

// Update at each frame the entity
void Enemy::update(double dt)
{
    // Update time
    m_stateTime += dt;

    // Call action
    m_actions[(int)m_currentState]();

    m_firstUpdateInState = false;

    if (m_nextState != EStates::None)
        applyChangeState();
}

// Draw at each frame the entity
void Enemy::draw(CHAR_INFO p_buffer[SCREEN_HEIGHT][SCREEN_WIDTH])
{
    // noop
}

// Set visual
void Enemy::setVisual(std::vector<std::string> p_visual)
{
    // noop
}

// Change current State
void Enemy::changeState(EStates p_newState)
{
    m_nextState = p_newState;
}

void Enemy::applyChangeState()
{
    // Debug
    Log::get()->write("[Enemy]:\tChanging to state " + std::to_string((int)m_nextState) + ".");

    m_currentState = m_nextState;
    m_nextState = EStates::None;
    m_stateTime = 0.f;
    m_firstUpdateInState = true;
}

// Grow the army
void Enemy::growArmy(void)
{
    m_nextWaveArmy = static_cast<size_t>(m_nextWaveArmy * ARMY_GROWING_FACTOR);
    m_nextWaveArmy += (int)(0.5f + m_nextWaveArmy * ARMY_RANDOM_COEFFICIENT);
}

// Destructor
Enemy::~Enemy()
{
}
