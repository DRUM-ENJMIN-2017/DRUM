#ifndef ENEMY_HPP
#define ENEMY_HPP

#include <vector>
#include <functional>

#include "Entity.hpp"

class Game;

/// Minimalist State Machine

// Idle duration
constexpr int MIN_IDLE_DURATION = 3;
constexpr int MAX_IDLE_DURATION = 10;

// Attack duration limits
constexpr int MIN_ATTACK_DURATION = 10;
constexpr int MAX_ATTACK_DURATION = 20;

// Gameplay
constexpr size_t BASE_ENEMY_ARMY = 7;
constexpr float ARMY_GROWING_FACTOR = 1.2f;
constexpr float ARMY_RANDOM_COEFFICIENT = 0.1f;
constexpr float ARMY_LOADING_COEFFICIENT = 0.45f;

// executeIdle(float dt)
typedef std::function<void()> stateFunction_t;

enum class EStates
{
    None = -1,

    Idle = 0,
    Attack_A,
    Attack_B,
    Load,

    Count
};

class Enemy : public Entity
{
public:
    // Constructor
    Enemy(Game& p_owner);

    // Update at each frame the entity
    void update(double dt) override;

    // Draw at each frame the entity
    void draw(CHAR_INFO p_buffer[SCREEN_HEIGHT][SCREEN_WIDTH]) override;

    // Set visual
    void setVisual(std::vector<std::string> p_visual) override;


    // Destructor
    virtual ~Enemy();

protected:
    void changeState(EStates p_newState);
    void applyChangeState();
    void growArmy(void);

    std::vector<stateFunction_t > m_actions;
    double m_stateTime;
    EStates m_currentState;
    EStates m_nextState;
    bool m_firstUpdateInState;

    int m_stateTimeout;

    Game& m_owner;

    size_t m_nextWaveArmy;
};

#endif // ENEMY_HPP