#include "Entity.hpp"


// Constructor
Entity::Entity(Scene& p_owner, size_t p_x, size_t p_y):
    m_owner(p_owner),
    m_x(p_x),
    m_y(p_y)
{
}

// Destructor
Entity::~Entity()
{
}
