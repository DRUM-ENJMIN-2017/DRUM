#ifndef ENTITY_HPP
#define ENTITY_HPP

#include "Globals.hpp"

#include <vector>

class Scene;

class Entity
{
public:
    // Constructor
	Entity(Scene& p_owner, size_t p_x, size_t p_y);
	
    // Update at each frame the entity
    virtual void update(double dt) = 0;

    // Draw at each frame the entity
    virtual void draw(CHAR_INFO p_buffer[SCREEN_HEIGHT][SCREEN_WIDTH]) = 0;
    
    // Set visual
    virtual void setVisual(std::vector<std::string> p_visual) = 0;

    // Destructor
	virtual ~Entity();

protected:
    std::vector<std::string> m_visual;
    Scene& m_owner;
    size_t m_x;
    size_t m_y;
};

#endif

