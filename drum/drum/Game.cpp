#include "Game.hpp"
#include "Resource.hpp"
#include "Mine.hpp"
#include "CapturePoint.hpp"
#include "GlobalTimerEntity.hpp"
#include "Enemy.hpp"
#include "Core.hpp"

#include <iostream>
#include <iomanip>
#include <sstream>
#include <cassert>
#include <algorithm>
#include <mutex>

#include <stdlib.h>

// Constructor
Game::Game(Core& p_owner) :
    Scene(p_owner),
    m_commandPrompt(*this), // Thread is active
    m_dialog(*this, 100ul, 0ul),
    m_enemy(nullptr)
{
    initialize();
}

// Initialize the Game
void Game::initialize(void)
{
    /// Add mines
    // Gold
    m_gameEntities.insert(std::make_pair(EEntities::GOLD, new Mine(*this, 2, 4, 200, 125, 75, 12, &Resource::get()->gold)));
    m_gameEntities[EEntities::GOLD]->setVisual({
" _____[]__",
"/    /   \\  ",
"/____/____\\ ",
"|[][]|||[]||| ",
"GOLD MINE     ",
"===========   " 
});

    // WOOD
    m_gameEntities.insert(std::make_pair(EEntities::WOOD, new Mine(*this, 32, 3, 200, 50, 95, 15, &Resource::get()->wood)));
    m_gameEntities[EEntities::WOOD]->setVisual( {
        " SAWMILL                (                         ",
        " =========              )   .^.                   ",
        "    ,^,         _______XX__'.'.')  .^.            ",
        ",^, .'.'        %%%%%%%%%%%'.'.(. '.`.'           ",
        "'.'.'.`.' ,^,   %%%%%%%%%%%',',X. '.'.',          ",
        "',','.'`%%%%%% | :H:H:H:H: | %%%%%%.',','.        ",
        "',',','. | : H : H : | : H : I'I:H:|:H:H:|.'.'.'. ",
        "!--!--!--!--!--!--!_!--!--!--!--!--!--!           "
    });

    // A control point
    m_gameEntities.insert(std::make_pair(EEntities::A_CONTROL_POINT, new CapturePoint(*this, 5, 15, 200, 75, 10)));
    m_gameEntities[EEntities::A_CONTROL_POINT]->setVisual({
        "   ______      ",
        "  /      \\     ",
        " /        \\    ",
        " |    A    |   ",
        " |  POINT  |   ",
        "  \\        /   ",
        "   \\_____/     ",
    });

    //// B control point
    m_gameEntities.insert(std::make_pair(EEntities::B_CONTROL_POINT, new CapturePoint(*this, 35, 15, 200, 75, 10)));
    m_gameEntities[EEntities::B_CONTROL_POINT]->setVisual({
        "   ______      ",
        "  /      \\     ",
        " /        \\    ",
        " |    B    |   ",
        " |  POINT  |   ",
        "  \\        /   ",
        "   \\_____/     ",
    });

    // Enemy
    m_gameEntities.insert(std::make_pair(EEntities::ENEMY, new Enemy(*this)));
    m_enemy = static_cast<Enemy*>(m_gameEntities[EEntities::ENEMY]);

    // Global Timer
    m_gameEntities.insert(std::make_pair(EEntities::GLOBAL_TIMER, new GlobalTimerEntity(*this, 100-25, 0)));

    // CommandPrompt
    m_commandPrompt.launchThread();
	//
	m_dialog.addMessage("Welcome in the world of DRUM", COLOR_TEXT_BLUE);
	m_dialog.addMessage("Your purpose is to live the longer you can.", COLOR_TEXT_BLUE);
	m_dialog.addMessage("Control your kingdom by sending order with the command prompt.", COLOR_TEXT_BLUE);
	m_dialog.addMessage("Type the command 'HELP' to see the command you can use.", COLOR_TEXT_BLUE);
	m_dialog.addMessage("--------------------------------------------------------------------", COLOR_TEXT_BLUE);
}

// Update
void Game::update(double dt)
{
	if (GetAsyncKeyState(VK_CONTROL) & 0x8000) // https://stackoverflow.com/questions/36870063/how-to-flush-or-clear-getasynckeystates-buffer
	{
		m_nextScene = EScenes::PauseScene;
		m_isActive = false;
		m_popOnExit = 0;

		m_commandPrompt.setGamePaused(true);
	}

	if (GetAsyncKeyState(VK_DOWN) & 0x8000)
	{
		m_commandPrompt.nextInput();
	}

	if (GetAsyncKeyState(VK_UP) & 0x8000)
	{
		m_commandPrompt.previousInput();
	}

	if (GetAsyncKeyState(VK_TAB) & 0x8000)
	{
		m_commandPrompt.autoCompleteInput();
	}
	
	if (m_commandPrompt.getGamePaused() && m_isActive)
	{
		m_commandPrompt.setGamePaused(false);
	}

    for (auto& pair : m_gameEntities) // c++11
    {
        assert(pair.second);
        pair.second->update(dt);
    }

    CapturePoint* a = static_cast<CapturePoint*>(m_gameEntities[EEntities::A_CONTROL_POINT]);
    CapturePoint* b = static_cast<CapturePoint*>(m_gameEntities[EEntities::B_CONTROL_POINT]);

    if ((a->isOccupied() && b->isOccupied()) || m_wantSurrender) // YOU LOOSE
    {
        m_isActive = false;
        m_nextScene = EScenes::LoseScene;
		m_popOnExit = 1;
		
		m_commandPrompt.killThread();
    }
}

// Draw all elements
void Game::draw(CHAR_INFO p_buffer[SCREEN_HEIGHT][SCREEN_WIDTH])
{
    // Clear buffer
    m_owner.clearBuffer();

    // Draw
    for (auto& pair : m_gameEntities)
    {
        assert(pair.second);
        pair.second->draw(p_buffer);
    }

    m_dialog.draw(p_buffer);

    /// Draw resources
    // GOLD: XXXX | WOOD: XXXX | HUMN: XXXX |
    // --------------------------------------
    //
    // Format string
    std::stringstream ressourceStrStream;
    ressourceStrStream
        << "GOLD  : " << std::setw(5) << std::to_string((int)Resource::get()->gold) << " | "
        << "WOOD  : " << std::setw(5) << std::to_string((int)Resource::get()->wood) << " | "
        << "HUMAN : " << std::setw(5) << std::to_string((int)Resource::get()->humans) << " | ";

    std::string line = ressourceStrStream.str();

    // Fill buffer with the string
    size_t size = line.size();

    for (size_t i = 0; i < size; i++)
    {
        p_buffer[0][i].Char.AsciiChar = line.at(i);
    }
    ///

    /// Draw human management
    std::stringstream humanStrStream;

    // Find max human recruitable
    int goldLimit = (int)(Resource::get()->gold / HUMAN_GOLD_COST);
    int woodLimit = (int)(Resource::get()->gold / HUMAN_WOOD_COST);
    //

    humanStrStream
        << "Human cost : " << HUMAN_GOLD_COST << " golds, " << HUMAN_WOOD_COST 
        << " woods, you can buy " << min(goldLimit, woodLimit) << " humans.";

    line = humanStrStream.str();

    // Fill buffer with the string
    size = line.size();

    for (size_t i = 0; i < size; i++)
    {
        p_buffer[1][i].Char.AsciiChar = line.at(i);
    }
            

    /// Add input content from commandPrompt
    std::mutex mutex;
    mutex.lock();
    std::string input = m_commandPrompt.getInput();
    mutex.unlock();

    // Add command prompt
    for(auto& rit = COMMAND_PROMPT.rbegin(); rit != COMMAND_PROMPT.rend() ; ++rit)
        input.insert(input.begin(), *rit);

    size = input.size();

    for (size_t i = 0; i < SCREEN_WIDTH; ++i)
    {
        if (i < size)
            p_buffer[SCREEN_HEIGHT - 1][i].Char.AsciiChar = input[i];
        else
            p_buffer[SCREEN_HEIGHT - 1][i].Char.AsciiChar = ' ';
    }
    ///
}

// Return an entity
Entity* Game::getEntity(EEntities p_key)
{
    if (m_gameEntities.find(p_key) == m_gameEntities.end())
    {
        Log::get()->write("[Game]:\tTried to get uninitialized entity : " + std::to_string(p_key) + ".");
        return nullptr;
    }

    return m_gameEntities.at(p_key);
}

// Exit
void Game::exit(void)
{
	m_popOnExit = 1;
	m_isActive = false;

    // Memory
    m_gameEntities.clear();

    // CommandPrompt
    m_commandPrompt.killThread();
}

// Destructor
Game::~Game()
{
    for (auto& pair : m_gameEntities)
    {
        delete pair.second;
        pair.second = nullptr;
    }

    m_gameEntities.clear();
}

const EScenes Game::getEnum(void) const
{
	return EScenes::GameScene;
}


