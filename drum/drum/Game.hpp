#ifndef GAME_HPP
#define GAME_HPP

#include <unordered_map>
#include <chrono> // c++11

#include "Scene.hpp"
#include "CommandPrompt.hpp"
#include "Dialog.hpp"

class Entity;
class Enemy;


class Game : public Scene
{
public:
    // Constructor
    explicit Game(Core& p_owner);
    
    // Entering 
    void initialize(void) override;

    // Update
    void update(double dt) override;

    // Draw
    void draw(CHAR_INFO p_buffer[SCREEN_HEIGHT][SCREEN_WIDTH]) override;

    // Exiting
    void exit(void) override;

	// Get Enum
	virtual const EScenes getEnum(void) const override;
    
	// Get entity
    Entity* getEntity(EEntities p_key);

    // Destructor
    virtual ~Game(void);
	
    // Returns the dialog
	Dialog& getDialog() { return m_dialog; }

	inline void surrender() { m_wantSurrender = true; }

protected:
    // Entities
	std::unordered_map<EEntities, Entity*> m_gameEntities;

    // Command Management
    CommandPrompt m_commandPrompt;

	// Dialog
	Dialog m_dialog;

    // Enemy
    Enemy* m_enemy;

	// Surrender ?
	bool m_wantSurrender = false;

};

#endif // GAME_HPP