#include "GlobalTimerEntity.hpp"

#include <iostream>
#include <sstream>
#include <iomanip>

#include "Scene.hpp"
#include "Core.hpp"

// Constructor
GlobalTimerEntity::GlobalTimerEntity(Scene& p_owner, size_t p_x, size_t p_y) : 
    Entity(p_owner, p_x,p_y)
{
}

// Update at each frame the entity
void GlobalTimerEntity::update(double dt)
{
    // noop
}

// Draw at each frame the entity
void GlobalTimerEntity::draw(CHAR_INFO p_buffer[SCREEN_HEIGHT][SCREEN_WIDTH])
{ 
    std::stringstream timerStream;
    timerStream << std::setw(15) 
                << "Time : "
                << std::to_string(m_owner.getOwner().getElapsedTime());

    std::string timer = timerStream.str();
    
    for(size_t i = 0 ; i < timer.size() ; ++i)
        p_buffer[m_y][m_x + i].Char.AsciiChar = timer[i];
}

// Set visual
void GlobalTimerEntity::setVisual(std::vector<std::string> p_visual)
{

}


// Destructor
GlobalTimerEntity::~GlobalTimerEntity()
{
}
