#ifndef GLOBALTIMERENTITY_HPP
#define GLOBALTIMERENTITY_HPP

#include "Entity.hpp"

class GlobalTimerEntity : public Entity
{
public:
    // Constructor
    GlobalTimerEntity(Scene& p_owner, size_t p_x, size_t p_y);

    // Update at each frame the entity
    void update(double dt) override;

    // Draw at each frame the entity
    void draw(CHAR_INFO p_buffer[SCREEN_HEIGHT][SCREEN_WIDTH]) override;

    // Set visual
    void setVisual(std::vector<std::string> p_visual) override;

    // Destructor
    virtual ~GlobalTimerEntity();
};

#endif // GLOBALTIMERENTITY_HPP