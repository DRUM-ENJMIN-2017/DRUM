#ifndef GLOBALS_HPP
#define GLOBALS_HPP

#include <CoreWindow.h>
#include "Log.hpp"
#include <string>

#define COLOR_TEXT_DARK_BLUE    0x01
#define COLOR_TEXT_GREEN        0x02
#define COLOR_TEXT_GREY_BLUE    0x03
#define COLOR_TEXT_MARRON       0x04
#define COLOR_TEXT_PURPLE       0x05
#define COLOR_TEXT_KAKI         0x06
#define COLOR_TEXT_LIGHT_GREY   0x07
#define COLOR_TEXT_GREY         0x08
#define COLOR_TEXT_BLUE         0x09
#define COLOR_TEXT_GREEN_FLUO   0x0A
#define COLOR_TEXT_BLUE_GREEN   0x0B
#define COLOR_TEXT_RED          0x0C
#define COLOR_TEXT_PINK         0x0D
#define COLOR_TEXT_YELLOW       0x0E
#define COLOR_TEXT_WHITE        0x0F

constexpr size_t MAX_ENTITY = 10;
constexpr size_t SCREEN_WIDTH = 192;
constexpr size_t SCREEN_HEIGHT = 50;
constexpr size_t CHAR_SIZE = 16;
constexpr float TRADE_COEFFICIENT = 0.8f;

// Units cost
constexpr size_t HUMAN_WOOD_COST = 25;
constexpr size_t HUMAN_GOLD_COST = 50;

const COORD BUFFER_COORD = { 0, 0 };
const COORD BUFFER_SIZE = { SCREEN_WIDTH, SCREEN_HEIGHT };

enum EEntities
{
	GOLD,
	WOOD,
	SOLDIER,
	A_CONTROL_POINT,
	B_CONTROL_POINT,
	DIALOG,
    ENEMY,
    GLOBAL_TIMER,

	COUNT
	
};

const std::string ENTITIES_TO_BUIDLING_STR[EEntities::COUNT] = {
	"GOLD MINE",          // GOLD,
	"SAWMILL",            // WOOD,
	"NEXUS",              // SOLDIER,
	"POINT A",            // A_CONTROL_POINT,
	"POINT B",            // B_CONTROL_POINT,
	"",                   // DIALOG,
    "",                   // ENEMY,
    "",                   // GLOBAL_TIMER,



};

const std::string ENTITIES_TO_RESSOURCE_STR[EEntities::COUNT] = {
	"GOLD",                 // GOLD,
    "WOOD",                 // WOOD,
    "SOLDIERS",             // SOLDIER,
    "",                     // A_CONTROL_POINT,
    "",                     // B_CONTROL_POINT,
    ""                      // DIALOG,
    ""                      // ENEMY,
    ""                      // GLOBAL_TIMER,
    ""
};



#endif // GLOBALS_HPP