#include "HelpScene.hpp"
#include "Core.hpp"

#define VK_KEY_R 0x52
HelpScene::HelpScene(Core& p_owner) :
	Scene(p_owner)
{
}


HelpScene::~HelpScene()
{
}

void HelpScene::initialize(void)
{
}

void HelpScene::update(double dt)
{
	if (GetAsyncKeyState(VK_KEY_R)  & 0x8000)
	{
		m_nextScene = EScenes::SplashscreenScene;
		m_popOnExit = 1;
		m_isActive = false;
	}

	FlushConsoleInputBuffer(GetStdHandle(STD_INPUT_HANDLE)); // flush input buffer
}

void HelpScene::draw(CHAR_INFO p_buffer[SCREEN_HEIGHT][SCREEN_WIDTH])
{
	size_t yOffset = (SCREEN_HEIGHT / 2 - HELPSCREEN.size() / 2);
	size_t xOffset = (SCREEN_WIDTH / 2 - HELPSCREEN[0].size() / 2);

	for (size_t y = yOffset; y < HELPSCREEN.size() + yOffset; ++y)
	{
		const std::string& line = HELPSCREEN[y - yOffset];

		for (size_t x = xOffset; x < line.size() + xOffset; ++x)
		{
			p_buffer[y][x].Char.AsciiChar = line[x - xOffset];
			p_buffer[y][x].Attributes = 0x0f;
		}
	}
}

void HelpScene::exit(void)
{
	m_popOnExit = 1;
	m_isActive = false;
}

const EScenes HelpScene::getEnum(void) const
{
	return EScenes::HelpScene;
}
