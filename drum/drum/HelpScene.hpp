#ifndef HELPSCENE_HPP
#define HELPSCENE_HPP

#include <string>
#include <vector>

#include "Scene.hpp"

class HelpScene :
	public Scene
{
public:
	HelpScene(Core& p_owner);
	virtual ~HelpScene();

	// H�rit� via Scene
	virtual void initialize(void) override;
	virtual void update(double dt) override;
	virtual void draw(CHAR_INFO p_buffer[SCREEN_HEIGHT][SCREEN_WIDTH]) override;
	virtual void exit(void) override;
	// Get Enum
	virtual const EScenes getEnum(void) const override;
	const std::vector<std::string> HELPSCREEN = {
		"============================================================",
		"#                                                          #",
		"#                         DRUM GAME                        #",
		"#                         ---------                        #",
		"#                            HELP                          #",
		"#                                                          #",
		"#  DRUM is a survival game. You are a lord that need to    #",
		"#  fight against enemies. You can control your kingdom by  #",
		"#  sending text orders.                                    #",
		"#                                                          #",
		"#  Commands :                                              #",
		"#    - RECRUIT <number of humans>                          #",
		"#    - MOVE <number of humans> FROM <A|B> TO <A|B>         #",
		"#    - DEFEND  <A | B> <number of humans>                  #",
		"#    - UPGRADE <GOLD|WOOD|A|B                              #",
		"#    - TRADE <number of ressource wanted> <WOOD|GOLD>      #",
		"#                                                          #",
		"#             - PRESS R TO RETURN TO MAIN MENU -           #",
		"#                                                          #",
		"============================================================"
	};
};

#endif

