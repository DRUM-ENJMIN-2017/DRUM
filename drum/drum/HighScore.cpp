#include "HighScore.hpp"
//#include "Log.hpp"

#include<fstream>
#include<string>
#include<sstream>
#include <algorithm>

HighScore* HighScore::instance;

// Singleton : getInstance
HighScore* HighScore::get(void)
{
	if (HighScore::instance == nullptr)
	{
		HighScore::instance = new HighScore;
	}

	return HighScore::instance;
}

// Singleton - Free
void HighScore::free(void)
{
	delete HighScore::instance;
}

void HighScore::loadHighScore()
{
	std::ifstream fileStream(m_highscorePath, std::ifstream::out);

	if (fileStream.is_open())
	{
		std::string line;
		std::string readingValue;
		std::string::size_type sz;
		
		while (std::getline(fileStream, line)) // we are getting the lines of the file
		{
			std::vector<std::string> tempValues;
			std::stringstream iss(line);

			while (std::getline(iss, readingValue, ';'))// we are splitting the line by ';'
			{
				tempValues.push_back(readingValue);
			}

			HighScorePlayerInfo infos;
			infos.playerName = tempValues[1];
			infos.score = std::stoi(tempValues[0], &sz);

			m_currentHighScores.push_back(infos);
		}
	}

	fileStream.close();
}

void HighScore::saveHighScore()
{
	std::ofstream fileStream(m_highscorePath, std::ofstream::out | std::ofstream::trunc);

	for (int i = 0; i < m_currentHighScores.size(); i++)
	{
		fileStream << m_currentHighScores[i].score << ";" << m_currentHighScores[i].playerName << std::endl;
	}

	fileStream.close();
}

bool HighScore::scoreIsHighScore(int p_score)
{
	bool isHighScore = false;
	int length = m_currentHighScores.size();

	for (int i = 0; i < length; i++)
	{
		if (m_currentHighScores[i].score < p_score)
		{
			return true;
		}
	}

	if (!isHighScore && length < MAX_HIGHSCORES)
	{
		isHighScore = true;
	}

	return isHighScore;
}

void HighScore::newHighScore(int p_score, std::string p_name)
{
	HighScorePlayerInfo currentPlayerInfo;
	currentPlayerInfo.playerName = p_name;
	currentPlayerInfo.score = p_score;

	int length = m_currentHighScores.size();

	if (length > 0)
	{
		int lastIndex = length - 1;
		for (int i = lastIndex; i >= 0; i--)
		{
			if (p_score > m_currentHighScores[i].score)
			{
				if (i + 1 < MAX_HIGHSCORES)
				{
					if (i+1 <= lastIndex)
					{
						m_currentHighScores[i + 1] = m_currentHighScores[i];
					}
					else {
						m_currentHighScores.push_back(m_currentHighScores[i]);
					}

					if (i == 0)
					{
						m_currentHighScores[i] = currentPlayerInfo;
						saveHighScore();
						return;
					}
				}
			}
			else 
			{
				if (i+1 < MAX_HIGHSCORES)
				{
					if (i + 1 <= lastIndex)
					{
						m_currentHighScores[i + 1] = currentPlayerInfo;
					}
					else {
						m_currentHighScores.push_back(currentPlayerInfo);
					}
					saveHighScore();
					return;
				}
			}
		}
	}
	else {
		m_currentHighScores.push_back(currentPlayerInfo);
		saveHighScore();
	}
	
}


HighScore::HighScore()
{
}


HighScore::~HighScore()
{
}
