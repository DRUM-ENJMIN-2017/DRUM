#ifndef HIGHSCORE_HPP
#define HIGHSCORE_HPP


const int MAX_HIGHSCORES =  10;


#include<vector>
#include<iostream>

class HighScorePlayerInfo {
public:
	std::string playerName;
	int score;

};

class HighScore
{
public:
	
	static HighScore* get();
	static void free();

	void loadHighScore();
	void saveHighScore();

	bool scoreIsHighScore(int p_score);
	void newHighScore(int p_score, std::string p_name);

	inline std::vector<HighScorePlayerInfo> getScores() const { return m_currentHighScores; }

	~HighScore();
	
private:
	static HighScore* instance;

	HighScore();
	
	const std::string m_highscorePath = "highscores.txt";

	std::vector<HighScorePlayerInfo> m_currentHighScores;
};

#endif //HIGHSCORE_HPP

