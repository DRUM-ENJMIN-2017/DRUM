#include "HighScoreScene.hpp"
#include "Core.hpp"
#include "HighScore.hpp"

#include<string>
#include<sstream>
#include<iomanip>


#define VK_KEY_M 0x4D

HighScoreScene::~HighScoreScene()
{
}

// Constructor
HighScoreScene::HighScoreScene(Core& p_owner) :
	Scene(p_owner)
{
	
	std::vector<HighScorePlayerInfo> scores = HighScore::get()->getScores();

	if (scores.size() > 0)
	{
		int rank = 1;
		for (std::vector<HighScorePlayerInfo>::iterator it = scores.begin(); it != scores.end(); ++it)
		{
			std::stringstream hsStrStream;
			hsStrStream << "#" << std::left << std::setw(2) << rank << " | SCORE : " << std::setw(3) << it->score << " | NOM : " << it->playerName;
			std::string line = hsStrStream.str();
			SCREEN.push_back(line);
			rank++;
		}
	}
	else {
		std::stringstream hsStrStream;
		hsStrStream << "NO HIGHSCORES FOUNDS";
		std::string line = hsStrStream.str();
		SCREEN.push_back(line);
	}

	SCREEN.insert(std::end(SCREEN), std::begin(FOOTER), std::end(FOOTER));
}

// Entering 
void HighScoreScene::initialize(void)
{

}

// Update
void HighScoreScene::update(double dt)
{
	// Space : Play game
	if (GetAsyncKeyState(VK_KEY_M) & 0x8000)
	{
		m_nextScene = EScenes::SplashscreenScene;
		m_popOnExit = 1;
		m_isActive = false;
	}
}

// Draw
void HighScoreScene::draw(CHAR_INFO p_buffer[SCREEN_HEIGHT][SCREEN_WIDTH])
{
	size_t yOffset = (SCREEN_HEIGHT / 2 - SCREEN.size() / 2);

	for (size_t y = yOffset; y < SCREEN.size() + yOffset; ++y)
	{
		const std::string& line = SCREEN[y - yOffset];
		size_t xOffset = (SCREEN_WIDTH / 2 - line.size() / 2);

		for (size_t x = xOffset; x < line.size() + xOffset; ++x)
		{
			p_buffer[y][x].Char.AsciiChar = line[x - xOffset];
			p_buffer[y][x].Attributes = COLOR_TEXT_WHITE;
		}
	}
}

const EScenes HighScoreScene::getEnum(void) const
{
	return EScenes::HighScoreScene;
}

// Exiting
void HighScoreScene::exit(void)
{
	m_popOnExit = 1;
	m_isActive = false;
}

