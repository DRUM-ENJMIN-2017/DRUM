#ifndef HIGHSCORESCENE_HPP
#define HIGHSCORESCENE_HPP

#include "Scene.hpp"

#include <vector>

class HighScoreScene :
	public Scene
{
public:
	HighScoreScene(Core& p_owner);
	~HighScoreScene();

	// Entering 
	void initialize(void) override;

	// Update
	void update(double dt) override;

	// Draw
	void draw(CHAR_INFO p_buffer[SCREEN_HEIGHT][SCREEN_WIDTH]) override;

	// Exiting
	void exit(void) override;

	// Get Enum
	virtual const EScenes getEnum(void) const override;

	std::vector<std::string> SCREEN = {
		" _    _ _____ _____ _    _  _____  _____ ____  _____  ______  _____ ",
		"| |  | |_   _/ ____| |  | |/ ____|/ ____/ __ \\|  __ \\|  ____|/ ____|",
		"| |__| | | || |  __| |__| | (___ | |   | |  | | |__) | |__  | (___  ",
		"|  __  | | || | |_ |  __  |\\___ \\| |   | |  | |  _  /|  __|  \\___ \\ ",
		"| |  | |_| || |__| | |  | |____) | |___| |__| | | \\ \\| |____ ____) |",
		"|_|  |_|_____\\_____|_|  |_|_____/ \\_____\\____/|_|  \\_\\______|_____/ ",
		"",
	};

	std::vector<std::string> FOOTER = {
		"",
		"M      :    Back to splashscreen",
	};
};

#endif

