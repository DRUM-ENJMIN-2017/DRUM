#include "Log.hpp"

Log* Log::instance;

// Constructor
Log::Log()
{
    // Open stream
    m_outputFile.open(OUTPUT_FILE, std::ofstream::out);

    // Chrono
    m_start = std::chrono::steady_clock::now();

    write("[Log]:\tLog starts recording...");

}

// Singleton - Get Instance
Log* Log::get(void)
{
    if (Log::instance == nullptr)
    {
        Log::instance = new Log;
    }

    return Log::instance;
}

// Singleton - Free
void Log::free(void)
{
    delete Log::instance;
}

// Write (rvalue) message
void Log::write(const std::string&& p_message, bool p_endl)
{
    size_t time = static_cast<size_t>((std::chrono::duration_cast<std::chrono::seconds>(std::chrono::steady_clock::now() - m_start)).count());
    m_outputFile << "[" << time << "s] : " << p_message.c_str();

    if (p_endl == true)
        m_outputFile << std::endl;
}

// Write (lvalue) message
void Log::write(const std::string& p_message, bool p_endl)
{
    size_t  time = static_cast<size_t>((std::chrono::duration_cast<std::chrono::seconds>(std::chrono::steady_clock::now() - m_start)).count());
    m_outputFile << "[" << time << "s] : " << p_message.c_str();

    if (p_endl == true)
        m_outputFile << std::endl;
}


// Destructor
Log::~Log()
{
    write("[Log]:\tEnd of recording.");
    m_outputFile.close();
}
