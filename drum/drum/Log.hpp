#ifndef LOG_HPP
#define LOG_HPP

#include <fstream>
#include <chrono>

#define OUTPUT_FILE "drum_debug.log"

class Log
{
public:
    // Singleton - get instance
    static Log* get(void);

    // Singleton - free instance
    static void free(void);

    // Write a message in debug
    void write(const std::string&& p_message, bool p_endl = true);
    void write(const std::string& p_message, bool p_endl = true);

    // Destructor
    virtual ~Log();

    private:
    // Constructor
    Log();

    static Log* instance;
    std::ofstream m_outputFile;
    std::chrono::time_point<std::chrono::steady_clock> m_start;

};

#endif // LOG_HPP