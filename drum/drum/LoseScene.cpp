#include "LoseScene.hpp"
#include "Core.hpp"
#include "HighScore.hpp"

#include <iostream>
#include <iomanip>
#include <sstream>

// Key code
#define VK_KEY_H 0x48
#define VK_KEY_M 0x4D

// Constructor
LoseScene::LoseScene(Core& p_owner, const int p_score) :
	Scene(p_owner),
	m_charHighScorePosition(0),
	m_score(p_score),
	m_dt(0)
{
	m_isHighScore = HighScore::get()->scoreIsHighScore(m_score);

	std::stringstream looseScoreStrStream;
	looseScoreStrStream << "YOU SURVIVED " << m_score << " SECONDS";
	std::string line = looseScoreStrStream.str();
	SCREEN.push_back(line);
}


// Entering 
void LoseScene::initialize(void)
{

}

// Update
void LoseScene::update(double dt)
{
	// Space : Play game
	if (GetAsyncKeyState(VK_KEY_M) & 0x8000)
	{
		m_nextScene = EScenes::SplashscreenScene;
		m_popOnExit = 1;
		m_isActive = false;

		if (m_isHighScore)
		{
			HighScore::get()->newHighScore(m_score, m_highScoreName);
		}
	}
	else if (GetAsyncKeyState(VK_LEFT) & 0x8000 && m_isHighScore)
	{
		m_charHighScorePosition--;
		if (m_charHighScorePosition < 0)
		{
			m_charHighScorePosition = 2;
		}
	}
	else if (GetAsyncKeyState(VK_RIGHT) & 0x8000 && m_isHighScore)
	{
		m_charHighScorePosition++;
		if (m_charHighScorePosition > 2)
		{
			m_charHighScorePosition = 0;
		}
	}
	else if (GetAsyncKeyState(VK_UP) & 0x8000 && m_isHighScore)
	{
		m_highScoreName[m_charHighScorePosition]++;
		if (m_highScoreName[m_charHighScorePosition] > 90)
		{
			m_highScoreName[m_charHighScorePosition] = 65;
		}

		HIGHSCORE[2][m_charHighScorePosition] = m_highScoreName[m_charHighScorePosition];
	}
	else if (GetAsyncKeyState(VK_DOWN) & 0x8000 && m_isHighScore)
	{
		m_highScoreName[m_charHighScorePosition]--;
		if (m_highScoreName[m_charHighScorePosition] < 65)
		{
			m_highScoreName[m_charHighScorePosition] = 90;
		}

		HIGHSCORE[2][m_charHighScorePosition] = m_highScoreName[m_charHighScorePosition];
	}

	m_dt += dt;
}

// Draw
void LoseScene::draw(CHAR_INFO p_buffer[SCREEN_HEIGHT][SCREEN_WIDTH])
{
	std::vector<std::string> finalDisplay;
	finalDisplay.insert(std::end(finalDisplay), std::begin(SCREEN), std::end(SCREEN));
	
	if (m_isHighScore)
	{
		finalDisplay.insert(std::end(finalDisplay), std::begin(HIGHSCORE), std::end(HIGHSCORE));
	}

	finalDisplay.insert(std::end(finalDisplay), std::begin(FOOTER), std::end(FOOTER));

	size_t yOffset = (SCREEN_HEIGHT / 2 - finalDisplay.size() / 2);

	for (size_t y = yOffset; y < finalDisplay.size() + yOffset; ++y)
	{
		const std::string& line = finalDisplay[y - yOffset];
		size_t xOffset = (SCREEN_WIDTH / 2 - line.size() / 2);

		for (size_t x = xOffset; x < line.size() + xOffset; ++x)
		{
			p_buffer[y][x].Char.AsciiChar = line[x - xOffset];

			// If we are drawing the 3rd line of HIGHSCORE '^^^' or 'vvv' 
			// at the x of m_charHighScorePosition
			if (m_isHighScore && (y == yOffset + SCREEN.size() + 1 || y == yOffset + SCREEN.size() + 3) && x == xOffset+m_charHighScorePosition)
			{
				// Make it blink
				p_buffer[y][x].Attributes = (int)(m_dt * 10) % 2 == 0
					? COLOR_TEXT_WHITE
					: 0x00;
			}
			else
			{
				p_buffer[y][x].Attributes = COLOR_TEXT_WHITE;
			}
		}
	}
}

const EScenes LoseScene::getEnum(void) const
{
	return EScenes::LoseScene;
}

// Exiting
void LoseScene::exit(void)
{
	m_popOnExit = 1;
	m_isActive = false;
}

// Destructor
LoseScene::~LoseScene()
{
}
