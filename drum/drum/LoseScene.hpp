#ifndef LOSESCENE_HPP
#define LOSESCENE_HPP

#include <string>
#include <vector>

#include "Scene.hpp"

class LoseScene : public Scene
{
public:
    // Constructor
    LoseScene(Core& p_owner, int p_score);

    // Entering 
    void initialize(void) override;

    // Update
    void update(double dt) override;

    // Draw
    void draw(CHAR_INFO p_buffer[SCREEN_HEIGHT][SCREEN_WIDTH]) override;

    // Exiting
    void exit(void) override;

	// Get Enum
	virtual const EScenes getEnum(void) const override;

    // Destructor
    virtual ~LoseScene();

	std::vector<std::string> SCREEN = {
		"   _____          __  __ ______    ______      ________ _____ ",
		"  / ____|   /\\   |  \\/  |  ____|  / __ \\ \\    / /  ____|  __ \\",
		" | |  __   /  \\  | \\  / | |__    | |  | \\ \\  / /| |__  | |__) |",
		" | | |_ | / /\\ \\ | |\\/| |  __|   | |  | |\\ \\/ / |  __| |  _  / ",
		" | |__| |/ ____ \\| |  | | |____  | |__| | \\  /  | |____| | \\ \\ ",
		"  \\_____/_/    \\_\\_|  |_|______|  \\____/   \\/   |______|_|  \\_\\",
		"                                                               ",
		
    };

	std::vector<std::string> HIGHSCORE = {
		"NEW HIGH SCORE, ENTER A NAME :",
		"^^^",
		"AAA",
		"vvv",
	};

	std::vector<std::string> FOOTER = {
		"",
		"M      :    Back to splashscreen",
	};



protected:
	bool m_isHighScore;
	int m_score;
	int m_charHighScorePosition; 
	double m_dt;
	char m_highScoreName[3] = {'A','A','A'};
	
};


#endif // LOSESCENE_HPP