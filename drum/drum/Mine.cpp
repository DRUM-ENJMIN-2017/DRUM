#include "Mine.hpp"
#include "Resource.hpp"

#include <cassert>

// Constructor
Mine::Mine(Scene& p_owner, size_t p_x, size_t p_y, float p_maxCapacity, size_t p_baseWoodCost, size_t p_baseGoldCost, float p_baseProduction, float* p_resource) :
	Building(p_owner, p_x, p_y,
		p_baseWoodCost,
		p_baseGoldCost),
	m_resource(p_resource),
	m_baseMaxCapacity(p_maxCapacity),
	m_baseProduction(p_baseProduction)
{
	assert(m_resource);
	*m_resource = 0;
}

// Update
void Mine::update(double dt)
{
	Building::update(dt);

	if (m_isActivated)
	{
		// Production
		if (m_processDuration > REFRESH_DURATION)
		{
			if (*m_resource < getMaxCapacity())
			{
				m_isFull = false;

				*m_resource += REFRESH_DURATION * (float)m_baseProduction * (float)std::pow(PRODUCTION_COEFFICIENT, m_level - 1);
				if (*m_resource > getMaxCapacity())
				{
					*m_resource = getMaxCapacity();
				}
			}
			else {
				m_isFull = true;
			}

			m_processDuration -= REFRESH_DURATION;
		}
	}
}

// Drawing
void Mine::draw(CHAR_INFO p_buffer[SCREEN_HEIGHT][SCREEN_WIDTH])
{
	m_visual[m_visual.size() - 3] = std::to_string((int)*m_resource) +
		"/" + std::to_string((int)getMaxCapacity());

	Building::draw(p_buffer);
}

// Set visual
void Mine::setVisual(std::vector<std::string> p_visual)
{
	Building::setVisual(p_visual);

	// Add the third line containing the resource
	m_visual.emplace_back();
}

// return current max capacity
float Mine::getMaxCapacity(void) const
{
	return m_baseMaxCapacity * static_cast<size_t>(std::pow(CAPACITY_COEFFICIENT, m_level - 1));
}

// Destructor
Mine::~Mine()
{
}
