#ifndef MINE_HPP
#define MINE_HPP

#include "Building.hpp"
#include "Globals.hpp"

#include <string>
#include <CoreWindow.h>

// Refresh
constexpr float REFRESH_DURATION = 0.5f;
constexpr float PRODUCTION_COEFFICIENT = 1.6f;
constexpr float CAPACITY_COEFFICIENT = 2.f;

class Mine : public Building
{
public:
    // Constructor
	explicit Mine(Scene& p_owner, size_t p_x, size_t p_y, float p_maxCapacity, 
         size_t p_baseWoodCost, size_t p_baseGoldCost, 
         float p_baseProduction, float* p_resource);

    // Building
	void update(double dt) override; //c++11
    void draw( CHAR_INFO p_buffer[SCREEN_HEIGHT][SCREEN_WIDTH]) override;
    void setVisual(std::vector<std::string> p_visual) override;

    float getMaxCapacity(void) const;

    // Destructor
	virtual ~Mine();

private:	
    // Is producing a resource
    float* m_resource;
    float m_baseMaxCapacity;
    float m_baseProduction;
};

#endif
