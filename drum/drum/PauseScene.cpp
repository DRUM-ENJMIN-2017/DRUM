#include "PauseScene.hpp"
#include "Core.hpp"

#define VK_KEY_R 0x52
#define VK_KEY_M 0x4D
PauseScene::PauseScene(Core& p_owner) :
	Scene(p_owner)
{
}


PauseScene::~PauseScene()
{
}

void PauseScene::initialize(void)
{
}

void PauseScene::update(double dt)
{
	if (GetAsyncKeyState(VK_KEY_M) & 0x8000) // https://stackoverflow.com/questions/36870063/how-to-flush-or-clear-getasynckeystates-buffer
	{
		m_nextScene = EScenes::SplashscreenScene;
		m_popOnExit = 2; // We need to pop the next scene -> the game scene
		m_isActive = false;
	}
	else if (GetAsyncKeyState(VK_KEY_R) & 0x8000) // https://stackoverflow.com/questions/36870063/how-to-flush-or-clear-getasynckeystates-buffer
	{
		m_nextScene = EScenes::None;
		m_popOnExit = 1;
		m_isActive = false;
	}

	FlushConsoleInputBuffer(GetStdHandle(STD_INPUT_HANDLE)); // flush input buffer
}

void PauseScene::draw(CHAR_INFO p_buffer[SCREEN_HEIGHT][SCREEN_WIDTH])
{
	size_t yOffset = (SCREEN_HEIGHT / 2 - PAUSESCREEN.size() / 2);
	size_t xOffset = (SCREEN_WIDTH / 2 - PAUSESCREEN[0].size() / 2);

	for (size_t y = yOffset; y < PAUSESCREEN.size()+ yOffset; ++y)
	{
		const std::string& line = PAUSESCREEN[y- yOffset];

		for (size_t x = xOffset; x < line.size()+ xOffset; ++x)
		{
			p_buffer[y][x].Char.AsciiChar = line[x-xOffset];
			p_buffer[y][x].Attributes = 0x0f;
		}
	}
}

void PauseScene::exit(void)
{
	m_popOnExit = 1;
	m_isActive = false;
}

const EScenes PauseScene::getEnum(void) const
{
	return EScenes::PauseScene;
}
