#ifndef PAUSESCENE_HPP
#define PAUSESCENE_HPP

#include <string>
#include <vector>

#include "Scene.hpp"

class PauseScene :
	public Scene
{
public:
	PauseScene(Core& p_owner);
	virtual ~PauseScene();

	// H�rit� via Scene
	virtual void initialize(void) override;
	virtual void update(double dt) override;
	virtual void draw(CHAR_INFO p_buffer[SCREEN_HEIGHT][SCREEN_WIDTH]) override;
	virtual void exit(void) override;
	// Get Enum
	virtual const EScenes getEnum(void) const override;
	const std::vector<std::string> PAUSESCREEN = {
		"============================================================",
		"#                                                          #",
		"#                         DRUM GAME                        #",
		"#                         ---------                        #",
		"#                           PAUSE                          #",
		"#                                                          #",
		"#                                                          #",
		"#              - PRESS M TO RETURN TO MAIN MENU -          #",
		"#              - PRESS R TO RETURN TO THE GAME -           #",
		"#                                                          #",
		"============================================================"
	};
};

#endif

