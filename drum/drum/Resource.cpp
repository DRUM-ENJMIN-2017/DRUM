#include "Resource.hpp"

#include <algorithm>

#include "Log.hpp"

Resource* Resource::instance = nullptr;


// Constructor
Resource::Resource() :
    gold(100),
    wood(100),
    humans(5)
{
}

// Singleton : getInstance
Resource* Resource::get(void)
{
    // First call - instance
	if (Resource::instance == nullptr)
	{
        Log::get()->write("[Resource]:\tCreating Singleton.");
        Resource::instance = new Resource;
	}

	return Resource::instance;
}

// Singleton : Free instance
void Resource::free(void)
{
	delete Resource::instance;
}

// Destructor
Resource::~Resource()
{
}
