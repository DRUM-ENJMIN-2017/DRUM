#ifndef RESOURCE_HPP
#define RESOURCE_HPP

class Resource
{
public:
    // Singleton : Get current instance
    static Resource* get(void);

    // Singleton : Free
    static void free(void);

    // Resource
    float gold;
    float wood;
    float humans;

    // Destructor
    virtual ~Resource();

protected:
    Resource();
        
    static Resource* instance;

	
	const int LIMIT_RES = 9999;
};

#endif // RESOURCE_HPP