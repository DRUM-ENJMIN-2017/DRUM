#include "Scene.hpp"

#include "Core.hpp"

// Constructor
Scene::Scene(Core& p_owner) :
    m_isActive(true),
    m_popOnExit(true),
    m_owner(p_owner),
    m_nextScene(EScenes::None)
{
}

// Destructor
Scene::~Scene()
{
}
