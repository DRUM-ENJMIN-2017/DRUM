#ifndef SCENE_HPP
#define SCENE_HPP

#include "Globals.hpp"

class Core;
enum class EScenes;

class Scene
{
public:
    // Constructor
    Scene(Core& p_owner);

    // Entering 
    virtual void initialize(void) = 0;
    
    // Update
    virtual void update(double dt) = 0;

    // Draw
    virtual void draw(CHAR_INFO p_buffer[SCREEN_HEIGHT][SCREEN_WIDTH]) = 0;

    // Exiting
    virtual void exit(void) = 0;

	// Get Enum
	virtual const EScenes getEnum(void) const = 0;

    // Is active
    inline bool isActive(void) const { return m_isActive; }
    
	inline void reactivate(void) { m_isActive = true; }

    // The number of scene we want to pop. f.e. : 1 means we want to pop
	// current scene on exit, 2 we pop the current scene + the one after
	// (usefull for scene like PauseScene)
    inline size_t mustPopOnExit(void) const {return m_popOnExit;}

    // Return the core
    inline const Core& getOwner(void) const {return m_owner;}

    // Returns the next scne
    inline EScenes getNextScene(void) const {return m_nextScene;}

    // Destructor
    virtual ~Scene();

protected:
    bool m_isActive;
    size_t m_popOnExit;
    Core& m_owner;
    EScenes m_nextScene;
};

#endif // SCENE_HPP