#include "SplashScreenScene.hpp"
#include "HighScore.hpp"
#include "Core.hpp"

// Key code
#define VK_KEY_H 0x48
#define VK_KEY_S 0x53
#define VK_KEY_R 0x52

// Constructor
SplashScreenScene::SplashScreenScene(Core& p_owner) :
    Scene(p_owner)
{
}

// Entering 
void SplashScreenScene::initialize(void)
{

}

// Update
void SplashScreenScene::update(double dt)
{
    // Space : Play game
    if (GetAsyncKeyState(VK_SPACE) & 0x8000)
    {
        m_nextScene = EScenes::GameScene;
        m_popOnExit = 1;
        m_isActive = false;
    }
    else if (GetAsyncKeyState(VK_KEY_H) & 0x8000) // https://stackoverflow.com/questions/36870063/how-to-flush-or-clear-getasynckeystates-buffer
    {
        m_nextScene = EScenes::HelpScene;
        m_popOnExit = 0;
        m_isActive = false;
    }
	if (GetAsyncKeyState(VK_KEY_S) & 0x8000)
	{
		m_nextScene = EScenes::HighScoreScene;
		m_popOnExit = 0;
		m_isActive = false;
	}
	if (GetAsyncKeyState(VK_KEY_R) & 0x8000)
	{
		if (HighScore::get()->scoreIsHighScore(150))
		{
			HighScore::get()->newHighScore(150, "AAA");
		}
	}
	FlushConsoleInputBuffer(GetStdHandle(STD_INPUT_HANDLE)); // flush input buffer
}

// Draw
void SplashScreenScene::draw(CHAR_INFO p_buffer[SCREEN_HEIGHT][SCREEN_WIDTH])
{
	size_t yOffset = (SCREEN_HEIGHT / 2 - SPLASHSCREEN.size() / 2);
	size_t xOffset = (SCREEN_WIDTH / 2 - SPLASHSCREEN[0].size() / 2);

	for (size_t y = yOffset; y < SPLASHSCREEN.size() + yOffset; ++y)
	{
		const std::string& line = SPLASHSCREEN[y - yOffset];

		for (size_t x = xOffset; x < line.size() + xOffset; ++x)
		{
			p_buffer[y][x].Char.AsciiChar = line[x - xOffset];
			p_buffer[y][x].Attributes = 0x0f;
		}
	}
}


const EScenes SplashScreenScene::getEnum(void) const
{
	return EScenes::SplashscreenScene;
}

// Exiting
void SplashScreenScene::exit(void)
{
	m_isActive = false;
	m_popOnExit = 0;
}

// Destructor
SplashScreenScene::~SplashScreenScene()
{
}
