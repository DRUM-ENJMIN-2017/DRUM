#ifndef SPLASHSCREENSCEEN_HPP
#define SPLASHSCREENSCEEN_HPP

#include <string>
#include <vector>

#include "Scene.hpp"

class SplashScreenScene : public Scene
{
public:
    // Constructor
    SplashScreenScene(Core& p_owner);

    // Entering 
    void initialize(void) override;

    // Update
    void update(double dt) override;

    // Draw
    void draw(CHAR_INFO p_buffer[SCREEN_HEIGHT][SCREEN_WIDTH]) override;

    // Exiting
    void exit(void) override;

	// Get Enum
	virtual const EScenes getEnum(void) const override;

    // Destructor
    virtual ~SplashScreenScene();

    const std::vector<std::string> SPLASHSCREEN = {
        "===============================================",
        "#                                             #",
        "#                    DRUM GAME                #",
        "#                    ---------                #",
        "#          Defend Recruit Upgrade Move        #",
        "#                                             #",
        "#                                             #",
        "#  Space  :    Launch the game                #",
		"#  H      :    Show Help and controls         #",
		"#  S      :    Show High Scores               #",
        "#  Escape :    Exit the game                  #",
        "#                                             #",
        "#                                             #",
        "==============================================="
    };

protected: 
};


#endif // SPLASHSCREENSCEEN_HPP