#include <iostream>

#include "Core.hpp"
#include "Resource.hpp"
#include "Globals.hpp"
#include "HighScore.hpp"

#include <windows.h>

// Main
int main()
{
    HWND window = GetConsoleWindow();
    HANDLE hConOut = GetStdHandle(STD_OUTPUT_HANDLE);
    
    // Force console size
    SMALL_RECT rectangle;
    rectangle.Left = 0;
    rectangle.Top = 0;
    rectangle.Right = SCREEN_WIDTH * CHAR_SIZE;
    rectangle.Bottom = SCREEN_HEIGHT * CHAR_SIZE;
    SetConsoleWindowInfo(hConOut, TRUE, &rectangle);

    COORD bufferSize;
    bufferSize.X = SCREEN_WIDTH * CHAR_SIZE;
    bufferSize.Y = SCREEN_HEIGHT * CHAR_SIZE;
    SetConsoleScreenBufferSize(hConOut, bufferSize);

    // Make sure to init Singleton
    Resource::get();
    Log::get();
	HighScore::get()->loadHighScore();
	
    Core core; // Launches game loop

    Resource::free();

    return EXIT_SUCCESS;
}